# Test 4 - Logic Test
## About

Berikut terdapat array berisi sederetan Strings.
['kita', 'atik', 'tika', 'aku', 'kia', 'makan', 'kua']
Silahkan kelompokkan/group kata-kata di dalamnya sesuai dengan kelompok Anagramnya.

## Install go(lang)

with [homebrew](http://mxcl.github.io/homebrew/) [mac]:

```Shell
sudo brew install go
```

with [apt](http://packages.qa.debian.org/a/apt.html)-get [linux]:

```Shell
sudo apt-get install golang
```

[install Golang manually](https://golang.org/doc/install)
or
[compile it yourself](https://golang.org/doc/install/source)

## Compile
that you can start go applications via 
```Shell
go run main.go
```
but also compile it to an executable with 
```Shell
go build main.go
```

## License
ISC
